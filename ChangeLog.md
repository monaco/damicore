
 ChangeLog
 ==============================

 Summary of changes beween versions.

 [0.1.0] - 2021-06-14
 ------------------------------
	
 Initial alpha release.


 [0.1.1] - 2021-06-20
 ------------------------------

 * Added 
 
  - dftools: lineshuffle, csv-colfiles
  - example directory
  
  
